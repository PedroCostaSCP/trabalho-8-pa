function ehd = calculateEHD(image)
    % Convert the image to grayscale
    grayImage = rgb2gray(image);
    
    % Apply edge detection using the Canny method
    edgeImage = edge(grayImage, 'canny');
    
    % Define the number of bins for the EHD histogram
    numBins = 80;
    
    % Calculate the histogram of edge orientations
    [Gmag, Gdir] = imgradient(edgeImage);
    edgeHist = imhist(Gdir, numBins);
    
    % Normalize the histogram
    edgeHist = edgeHist / sum(edgeHist);
    
    % Combine the magnitude and direction histograms into a single feature vector
    ehd = [imhist(Gmag, numBins); edgeHist];
end

