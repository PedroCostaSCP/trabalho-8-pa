function lone=getL1_EHD(ehd1,ehd2)
    L1 = sum(abs(ehd1-ehd2));
    lone=L1;
end