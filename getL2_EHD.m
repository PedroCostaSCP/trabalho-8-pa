function ltwo=getL2_EHD(ehd1,ehd2)
    L2 = sqrt(sum((ehd1-ehd2).^2))
    ltwo=L2
end