function distance=calculateWeightedDistance(image1, image2)
            cld1 = findcld(image1)
            cld2 = findcld(image2)
            
            cldY1=cld1(1:64); cldCb1=cld1(65:128); cldCr1=cld1(129:192);
            cldY2=cld2(1:64); cldCb2=cld2(65:128); cldCr2=cld2(129:192);
            
            Dw = sqrt(sum(2*((cldY1-cldY2).^2))) + sqrt(sum(2*((cldCb1-cldCb2).^2))) + sqrt(sum(4*((cldCr1-cldCr2).^2)));
            
            distance = Dw            
        end