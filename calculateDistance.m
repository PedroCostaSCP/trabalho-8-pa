function distance = calculateDistance(ehd1, ehd2)
    % Calculate the Euclidean distance between two EHD feature vectors
    distance = norm(ehd1 - ehd2);
end